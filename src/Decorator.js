import React, { Component, } from "react";
import { CompositeDecorator, Editor, EditorState } from 'draft-js';

export default class Decorator extends Component {


    constructor() {
        super();
        const compositeDecorator = new CompositeDecorator([
            {
                strategy: handleStrategy,
                component: HandleSpan,
            },
            {
                strategy: hashtagStrategy,
                component: HashtagSpan,
            },
        ]);

        this.state = {
            editorState: EditorState.createEmpty(compositeDecorator),
        };

        this.focus = () => this.refs.editor.focus();
        this.onChange = (editorState) => this.setState({ editorState });
        this.logState = () => console.log(this.state.editorState.toJS());
        this._onDrop = this._onDrop.bind(this);
    }


    _onDrop(event) {
        // this.setState({ className: '' });
        debugger
        event.preventDefault()
        try {
            var itemId = event.dataTransfer.getData('itemId');
            var cuurentItem = event.target;
            var itemData = document.getElementById(itemId);

            let range = null;

            range = document.caretRangeFromPoint(event.clientX, event.clientY);

            let sel = window.getSelection();

            sel.removeAllRanges(); 
    		sel.addRange( range );
    		event.target.focus();
    		
    		document.execCommand('insertHTML',false, itemData.innerHTML);
    		sel.removeAllRanges();
            
            itemData.remove();
            // itemData.parentElement.removeChild(itemData)

            // cuurentDiv.parentElement.insertBefore(itemData, cuurentDiv);
            // cuurentItem.appendChild(itemData);

        } catch (e) {
            return
        }
        return false
    }

    render() {
        return (
            <div style={styles.root}>
                <div style={styles.editor}
                    onDragEnterCapture={(event) => {

                    }}
                    onDragOver={(event) => { event.preventDefault(); }}
                    onDrop={this._onDrop}
                    // onDragEnd={(event) => {
                    //     debugger
                    // }}

                    onClick={this.focus}>
                    <Editor
                        editorState={this.state.editorState}
                        onChange={this.onChange}
                        placeholder="Write a tweet..."
                        ref="editor"
                        spellCheck={true}

                    />
                </div>
                <input
                    onClick={this.logState}
                    style={styles.button}
                    type="button"
                    value="Log State"
                />
            </div>
        );
    }
}


/**
       * Super simple decorators for handles and hashtags, for demonstration
       * purposes only. Don't reuse these regexes.
       */
const HANDLE_REGEX = /\@[\w]+/g;
const HASHTAG_REGEX = /\#[\w\u0590-\u05ff]+/g;

function handleStrategy(contentBlock, callback, contentState) {
    findWithRegex(HANDLE_REGEX, contentBlock, callback);
}

function hashtagStrategy(contentBlock, callback, contentState) {
    findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}

function findWithRegex(regex, contentBlock, callback) {
    const text = contentBlock.getText();
    let matchArr, start;
    while ((matchArr = regex.exec(text)) !== null) {
        start = matchArr.index;
        callback(start, start + matchArr[0].length);
    }
}

const HandleSpan = (props) => {
    return (
        <span
            style={styles.handle}
            data-offset-key={props.offsetKey}
        >
            {props.children}
        </span>
    );
};

const HashtagSpan = (props) => {
    //on drag start handler
    const onDragStartHandler = event => {
        event.dataTransfer.setData('itemId', event.target.id)
    }

    return (
        <a
            id={`Title_${props.offsetKey}`}
            name={`Title_${props.offsetKey}`}
            style={styles.hashtag}
            data-offset-key={props.offsetKey}
            onDragStart={onDragStartHandler}
            draggable={true}
            style={{ cursor: 'pointer', padding: '10px' }}
        >
            {props.children}
        </a>
    );
};

const styles = {
    root: {
        fontFamily: '\'Helvetica\', sans-serif',
        padding: 20,
        width: 600,
    },
    editor: {
        border: '1px solid #ddd',
        cursor: 'text',
        fontSize: 16,
        minHeight: 40,
        padding: 10,
    },
    button: {
        marginTop: 10,
        textAlign: 'center',
    },
    handle: {
        color: 'rgba(98, 177, 254, 1.0)',
        direction: 'ltr',
        unicodeBidi: 'bidi-override',
    },
    hashtag: {
        color: 'rgba(95, 184, 138, 1.0)',
    },
};