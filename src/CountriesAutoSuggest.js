import React, { Component, } from "react";




export class CountriesAutoSuggest extends Component {

    constructor(props) {
        super(props);


        this.state = {
            countrySearchValue: "",
            isShowCountries: false,
        }

        this.countryOnChangeHandler = this.countryOnChangeHandler.bind(this);
        this.onFocusCountrySearchInput = this.onFocusCountrySearchInput.bind(this);
        this.onCountrySelectedClick = this.onCountrySelectedClick.bind(this);
        this.getCountriesSuggestions = this.getCountriesSuggestions.bind(this);

    }



    onFocusCountrySearchInput = (event) => {
        this.setState({
            isShowCountries: true
        });
    }

    countryOnChangeHandler = e => {

        // Filter our suggestions that don't contain the user's input
        // const filteredSuggestions = suggestions.filter(
        //     suggestion =>
        //         suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        // );

        this.setState({
            countrySearchValue: e.currentTarget.value,
            isShowCountries: true
        });

    };


    getCountriesSuggestions = (countrySearchValue) => {

        const { countries } = this.props;

        if (countrySearchValue !== "") {
            const escapedValue = escapeRegexCharacters(countrySearchValue.trim());

            if (escapedValue === '') {
                return [];
            }

            const regex = new RegExp('\\b' + escapedValue, 'i');

            const searchedCountries = countries.filter(country => regex.test(getSuggestionValue(country)));

            return searchedCountries;
        }


        return countries;

    }

    onCountrySelectedClick = (event, selectedCountry) => {

        this.setState({
            isShowCountries: false,
            countrySearchValue: selectedCountry.name,
        });

        this.props.onCountrySelectionClickHandler(event, selectedCountry);

    }



    render() {

        const { isShowCountries, countrySearchValue } = this.state

        let countrySuggestions = this.getCountriesSuggestions(countrySearchValue);

        return (

            <div>
                <input
                    className=""
                    type="text"
                    onChange={this.countryOnChangeHandler}
                    value={countrySearchValue}
                    onFocus={this.onFocusCountrySearchInput}
                    placeholder="Search Countries"
                />


                {
                    isShowCountries ?
                        <ul>
                            {
                                countrySuggestions && countrySuggestions.map((country, idx) => {

                                    return (
                                        <li>
                                            <div onClick={(event) => this.onCountrySelectedClick(event, country)}>
                                                {country.name}
                                            </div>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                        : ""
                }

            </div>
        )
    }
}

//https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestionValue(suggestion) {
    return `${suggestion.dial_code} ${suggestion.name}`;
}