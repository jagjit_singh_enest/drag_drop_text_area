import React, { Component, } from "react";
import IsolatedScroll from 'react-isolated-scroll';



class Autocomplete extends Component {

    constructor(props) {
        super(props);

        this.highlightedItem = null;

        this.state = {
            // The active selection's index
            activeSuggestion: 0,
            // The suggestions that match the user's input
            filteredSuggestions: [],
            // Whether or not the suggestion list is shown
            showSuggestions: false,
            // What the user has entered
            userInput: "",
            //Open a items container
            isOpen: false,
        };



        this.onFocusSearchInput = this.onFocusSearchInput.bind(this);
        this.onBlurSearchInput = this.onBlurSearchInput.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.renderItemsContainer = this.renderItemsContainer.bind(this);
        this.storeItemsContainerReference = this.storeItemsContainerReference.bind(this);
        this.ensureHighlightedItemIsVisible = this.ensureHighlightedItemIsVisible.bind(this);
        this.onHighlightedItemChange = this.onHighlightedItemChange.bind(this);
        this.storeHighlightedItemReference = this.storeHighlightedItemReference.bind(this);

    }

    componentDidMount() {
        console.log("componentDidMount")
        this.ensureHighlightedItemIsVisible();
    }

    componentDidUpdate() {
        console.log("componentDidUpdate")
        this.ensureHighlightedItemIsVisible();
    }

    renderItemsContainer = ({ children, containerProps }) => {
        // const { ref, ...restContainerProps } = containerProps;
        // const callRef = isolatedScroll => {
        //     if (isolatedScroll !== null) {
        //         ref(isolatedScroll.component);
        //     }
        // };

        // return (
        //     <IsolatedScroll ref={callRef} {...restContainerProps}>
        //         {children}
        //     </IsolatedScroll>
        // );

        return <div {...containerProps}>{children}</div>

        // return <div ref={callRef} {...restContainerProps}>{children}</div>
    }

    storeHighlightedItemReference = highlightedItem => {
        this.onHighlightedItemChange(highlightedItem === null ? null : highlightedItem);
    }

    onHighlightedItemChange = highlightedItem => {
        this.highlightedItem = highlightedItem;
    }

    ensureHighlightedItemIsVisible = () => {
        const { highlightedItem } = this;

        if (!highlightedItem) {
            return;
        }

        const { itemsContainer } = this;
        const itemOffsetRelativeToContainer =
            highlightedItem.offsetParent === itemsContainer
                ? highlightedItem.offsetTop
                : highlightedItem.offsetTop - itemsContainer.offsetTop;

        let { scrollTop } = itemsContainer; // Top of the visible area

        if (itemOffsetRelativeToContainer < scrollTop) {
            // Item is off the top of the visible area
            scrollTop = itemOffsetRelativeToContainer;
        } else if (itemOffsetRelativeToContainer + highlightedItem.offsetHeight > scrollTop + itemsContainer.offsetHeight) {
            // Item is off the bottom of the visible area
            scrollTop = itemOffsetRelativeToContainer + highlightedItem.offsetHeight - itemsContainer.offsetHeight;
        }

        if (scrollTop !== itemsContainer.scrollTop) {
            itemsContainer.scrollTop = scrollTop;
        }

        console.log("scrollTop", scrollTop)
    }

    storeItemsContainerReference = itemsContainer => {
        if (itemsContainer !== null) {
            this.itemsContainer = itemsContainer;
        }
    }


    onFocusSearchInput = (event) => {
        // this.onChange(event);
    }


    onBlurSearchInput = (event) => {
        // this.setState({
        //     isOpen: false
        // });
    }

    onChange = e => {
        const { suggestions } = this.props;
        const userInput = e.currentTarget.value;

        // Filter our suggestions that don't contain the user's input
        const filteredSuggestions = suggestions.filter(
            suggestion =>
                suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        );

        this.setState({
            activeSuggestion: 0,
            filteredSuggestions,
            showSuggestions: true,
            userInput: e.currentTarget.value,
            isOpen: true
        });
    };

    onClick = e => {
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: e.currentTarget.innerText
        });
    };

    onKeyDown = e => {
        const { activeSuggestion, filteredSuggestions } = this.state;

        // User pressed the enter key
        if (e.keyCode === 13) {
            this.setState({
                activeSuggestion: 0,
                showSuggestions: false,
                userInput: filteredSuggestions[activeSuggestion]
            });
            e.preventDefault(); // Prevents the cursor from moving
        }
        // User pressed the up arrow
        else if (e.keyCode === 38) {
            if (activeSuggestion === 0) {
                return;
            }

            this.setState({
                activeSuggestion: activeSuggestion - 1,
                userInput: filteredSuggestions[activeSuggestion - 1]
            });

            e.preventDefault(); // Prevents the cursor from moving
        }
        // User pressed the down arrow
        else if (e.keyCode === 40) {
            if (activeSuggestion - 1 === filteredSuggestions.length) {
                return;
            }



            this.setState({
                activeSuggestion: activeSuggestion + 1,
                userInput: filteredSuggestions[activeSuggestion + 1]
            });

            e.preventDefault(); // Prevents the cursor from moving

        }


    };

    render() {
        const {
            onChange,
            onClick,
            onKeyDown,
            state: {
                activeSuggestion,
                filteredSuggestions,
                showSuggestions,
                userInput,
                isOpen
            }
        } = this;

        let suggestionsListComponent;

        if (showSuggestions && (userInput || isOpen === true)) {
            if (filteredSuggestions.length) {
                suggestionsListComponent = (

                    <ul className="react-autosuggest__suggestions-list">
                        {filteredSuggestions.map((suggestion, index) => {

                            let isHighlighted = false
                            // Flag the active suggestion with a class
                            if (index === activeSuggestion) {
                                className = "suggestion-active";
                                isHighlighted = true
                            }

                            const allItemProps = {
                                key: suggestion,
                            };

                            if (isHighlighted) {
                                allItemProps.ref = this.storeHighlightedItemReference;
                            }


                            let className;



                            return (
                                <li
                                    {...allItemProps}
                                    className={`react-autosuggest__suggestion ${className}`}
                                    key={suggestion}
                                    onClick={onClick}>
                                    <div>{suggestion}</div>
                                </li>
                            );
                        })}
                    </ul>

                );
            } else {
                suggestionsListComponent = (
                    <div className="no-suggestions">
                        <em>No suggestions, you're on your own!</em>
                    </div>
                );
            }
        }

        const itemsContainerId = `react-autowhatever-search`;

        const itemsContainer = this.renderItemsContainer({
            containerProps: {
                className: `react-autosuggest__suggestions-container ${
                    isOpen ? 'react-autosuggest__suggestions-container--open' : ''
                    }`,

                id: itemsContainerId,
                role: 'listbox',
                ref: this.storeItemsContainerReference
            },
            children: suggestionsListComponent
        });


        return (

            <div className="react-autosuggest__container" >
                <div className="inputContainer">
                    <input
                        className="form-control search search-box padding-right-25"
                        type="text"
                        onChange={onChange}
                        onKeyDown={onKeyDown}
                        value={userInput}
                        onFocus={this.onFocusSearchInput}
                        onBlur={this.onBlurSearchInput}
                    />
                </div>
                {itemsContainer}
            </div>

        );
    }
}

export default Autocomplete;
