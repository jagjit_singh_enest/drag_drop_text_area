import React, { Component } from 'react';
import htmlToDraft from 'html-to-draftjs'
import { EditorState, ContentState, convertFromHTML, convertToRaw } from 'draft-js';
import logo from './logo.svg';
import './App.css';
import WysiwygEditor from './Editor';
import draftToHtml from 'draftjs-to-html'
import Autocomplete from './AutoComplete';
import { CountriesAutoSuggest } from './CountriesAutoSuggest';
import { countries } from "./countriesJson";
import Decorator from './Decorator';
import DragAndDropTextArea from './DragAndDropTextArea';

const content = {
  "entityMap": {},
  "blocks": [{
    "key": "637gr", "text": "Initialized from content state.",
    "type": "unstyled", "depth": 0, "inlineStyleRanges": [], "entityRanges": [], "data": {}
  }],
  bold: false,
  italic: true,
  monospace: true,
  strikethrough: true,
  subscript: false,
  superscript: false,
  underline: true,
};

// let html = '<p style="text-align:right;"><span style="color: rgb(0,168,226);"><code><del><em><ins>Thi</ins></em></del></code></span><span style="color: rgb(0,168,226);font-family: Times New Roman;"><code><del><em><ins>s i</ins></em></del></code></span><span style="color: rgb(0,168,226);font-size: 14px;font-family: Times New Roman;"><code><del><em><ins>s a Title</ins></em></del></code></span></p><p>&nbsp;</p><blockquote style="text-align:center;"><span style="background-color: rgb(255,136,122);font-size: 48px;"><strong>s</strong><sub><strong>d</strong></sub><strong>fds</strong><sup><strong>fsdf sdfdsfdf</strong></sup></span></blockquote><p><span style="background-color: rgb(255,136,122);font-size: 48px;"><sup><strong>dsfsdfdsf dsf dsf sdf sdf</strong></sup></span></p><p><span style="background-color: rgb(255,136,122);font-size: 48px;"><sup><strong>dsfsdf</strong></sup></span></p>'
// let html = '<p style="text-align: right; word-break: break-word; margin: 0px; white-space: pre-wrap;"><span style="color: rgb(255,136,122);background-color: rgb(255,208,222);font-size: 36px;font-family: Great Vibes;"><strong>This is a Title</strong></span></p><p style="word-break: break-word; margin: 0px; white-space: pre-wrap;"><span style="color: rgb(255,136,122);background-color: rgb(255,208,222);font-size: 36px;font-family: Abril Fatface;"><strong>sd</strong></span><span style="color: rgb(255,136,122);background-color: rgb(255,208,222);font-size: 42px;font-family: Abril Fatface;"><strong>fsd</strong></span><span style="color: rgb(255,136,122);background-color: rgb(255,208,222);font-size: 36px;font-family: Abril Fatface;"><strong>fsd</strong></span><span style="font-size: 16px;">sdfsfdsf</span><span style="font-family: Lobster;">sdfsdfdsfsdf</span><span style="font-size: 22px;">sdfsdfsd</span></p><p style="text-align: center; word-break: break-word; margin: 0px; white-space: pre-wrap;"><span style="font-size: 22px;">sdf<strong>sfdsf</strong><strong><em>sdfsdf</em></strong><strong><em><ins>sfsdfs</ins></em></strong></span></p>'
// let html = '<p><span style="line-height: 2.5"><strong style="font-weight: bold">This is a Title</strong></span></p><p><strong style="font-weight: bold">sd</strong><strong style="font-weight: bold">fsd</strong><strong style="font-weight: bold">fsd</strong>sdfsfdsfsdfsdfdsfsdfsdfsdfsd</p><p>sdf<strong style="font-weight: bold">sfdsf</strong><em style="font-style: italic"><strong style="font-weight: bold">sdfsdf</strong></em><u style="text-decoration: underline"><em style="font-style: italic"><strong style="font-weight: bold">sfsdfs</strong></em></u></p><p><u style="text-decoration: underline"><em style="font-style: italic"><strong style="font-weight: bold">fddsfsdfsdfdsf</strong></em></u></p><p><u style="text-decoration: underline"><em style="font-style: italic"><strong style="font-weight: bold">sdfsdfdsf sdf</strong></em></u></p><p><u style="text-decoration: underline"><em style="font-style: italic"><strong style="font-weight: bold">sdfd</strong></em></u></p><p><u style="text-decoration: underline"><em style="font-style: italic"><strong style="font-weight: bold">sf sdf sd dsf sdf</strong></em></u></p><p>asdsadasd asd asdasdasdas</p>'
let html = '<p style="text-align:Left; line-height:1.40;"><span style="font-size: 16px;"></span></p>';
class App extends Component {

  constructor(props) {
    super(props);



    let editorState = null

    const blocksFromHtml = htmlToDraft(html)
    const { contentBlocks, entityMap } = blocksFromHtml
    const contentState = ContentState.createFromBlockArray(
      contentBlocks,
      entityMap
    )

    editorState = EditorState.createWithContent(contentState)

    this.state = {
      editorState: editorState //EditorState.createEmpty(),
    }


    this.onCountrySelectionClickHandler = this.onCountrySelectionClickHandler.bind(this);

  }



  onEditorStateChange = (editorState) => {
    this.setState({
      editorState,
    });

  };




  //
  onCountrySelectionClickHandler = (event, selectedCountry) => {

    console.log("country", selectedCountry)

  }



  render() {
    const { editorState } = this.state;



    return (
      <div className="App">
        <header className="App-header">
          {/* <WysiwygEditor
            editorState={editorState}
            wrapperClassName="demo-wrapper"
            editorClassName="demo-editor"
            onEditorStateChange={this.onEditorStateChange}
          />

          <textarea
            disabled
            value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
          /> */}

          {/* <Autocomplete
            suggestions={[
              "Alligator",
              "Bask",
              "Crocodilian",
              "Death Roll",
              "Eggs",
              "Jaws",
              "Reptile",
              "Solitary",
              "Tail",
              "Wetlands"
            ]}
          /> */}

          {/* <CountriesAutoSuggest
            countries={countries}
            onCountrySelectionClickHandler={this.onCountrySelectionClickHandler}
          /> */}
          {/* <Decorator /> */}
          <DragAndDropTextArea />
        </header>
      </div>
    );
  }
}

export default App;
