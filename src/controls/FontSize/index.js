/* @flow */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LayoutComponent from './Component';
import {
  toggleCustomInlineStyle
} from 'draftjs-utils'
import {
  getSelectionCustomInlineStyle,
} from '../../js/inline';

export default class FontSize extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    editorState: PropTypes.object,
    modalHandler: PropTypes.object,
    config: PropTypes.object,
    translations: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      expanded: undefined,
      currentFontSize: undefined,
    };


    this.toggleFontSize = this.toggleFontSize.bind(this);
    this.onExpandEvent = this.onExpandEvent.bind(this);
    this.doExpand = this.doExpand.bind(this);
    this.doCollapse = this.doCollapse.bind(this);
    this.expandCollapse = this.expandCollapse.bind(this);
  }



  componentWillMount() {
    const { editorState, modalHandler } = this.props;
    if (editorState) {
      this.setState({
        currentFontSize:
          getSelectionCustomInlineStyle(editorState, ['FONTSIZE']).FONTSIZE,
      });
    }
    modalHandler.registerCallBack(this.expandCollapse);
  }

  componentWillReceiveProps(properties) {
    if (properties.editorState &&
      this.props.editorState !== properties.editorState) {
      this.setState({
        currentFontSize:
          getSelectionCustomInlineStyle(properties.editorState, ['FONTSIZE']).FONTSIZE,
      });
    }
  }

  componentWillUnmount() {
    const { modalHandler } = this.props;
    modalHandler.deregisterCallBack(this.expandCollapse);
  }

  onExpandEvent() {
    this.signalExpanded = !this.state.expanded;
  };

  expandCollapse() {
    this.setState({
      expanded: this.signalExpanded,
    });
    this.signalExpanded = false;
  }

  doExpand() {
    this.setState({
      expanded: true,
    });
  };

  doCollapse() {
    this.setState({
      expanded: false,
    });
  };

  toggleFontSize(fontSize) {
    const { editorState, onChange } = this.props;
    const newState = toggleCustomInlineStyle(
      editorState,
      'fontSize',
      fontSize,
    );
    if (newState) {

      console.log('font Size aa ', getSelectionCustomInlineStyle(newState, ['FONTSIZE']).FONTSIZE)
      onChange(newState);
    }
  };

  render() {
    const { config, translations } = this.props;
    const { expanded, currentFontSize } = this.state;
    const FontSizeComponent = config.component || LayoutComponent;
    const fontSize = currentFontSize && Number(currentFontSize.substring(9));
    return (
      <FontSizeComponent
        config={config}
        translations={translations}
        currentState={{ fontSize }}
        onChange={this.toggleFontSize}
        expanded={expanded}
        onExpandEvent={this.onExpandEvent}
        doExpand={this.doExpand}
        doCollapse={this.doCollapse}
      />
    );
  }
}
