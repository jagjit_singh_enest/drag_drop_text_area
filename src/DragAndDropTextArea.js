import React, { Component, } from "react";

export default class DragAndDropTextArea extends Component {

    constructor() {
        super();


        this.state = {
            messageText: "",
            blockVal: 0,
        };

        this.addNewLink = this.addNewLink.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }


    componentDidMount() {

    }

    handleChange = (event) => {

        //console.log('inputText', event.target.innerText)

        // const isValid = isValidMessageText(event.target.innerText.trim());

        this.setState({
            messageText: event.target.innerText
        })
    }



    addNewLink = () => {

        var native_receiverElement = document.getElementById("native_receiver")

        //remove extra line break tag
        var lineBreakTags = native_receiverElement.getElementsByTagName("br")

        if (lineBreakTags && lineBreakTags.length > 0) {
            lineBreakTags[lineBreakTags.length - 1].remove();
        }


        var node = document.createElement("span");

        var emptyNode = document.createElement("span");
        var emptyText = document.createTextNode("\u00A0");

        emptyNode.appendChild(emptyText);

        native_receiverElement.appendChild(emptyNode)

        var textnode = document.createTextNode(`Block ${this.state.blockVal + 1}`);

        node.draggable = true
        node.className = "native_drag"
        node.contentEditable = false
        node.style = "display: inline-flex;margin-left: 6px;margin-right: 6px;border-radius: 2px;background: #EDF5FF;padding:4px;font-family: Open Sans;font-style: normal;font-weight: normal;font-size: 14px;line-height: 13px;text-align: center;color: #3AA6DD;"

        node.appendChild(textnode);
        native_receiverElement.appendChild(node)

        nativeBindDraggable();

        nativeBindWriteRegion();

        var lineBreakNode = document.createElement("br");

        native_receiverElement.appendChild(lineBreakNode)

        this.setState({
            blockVal: this.state.blockVal + 1
        })
    }


    render() {

        const isValid = isValidMessageText(this.state.messageText.trim());

        return (
            <div>
                <div
                    id="native_receiver"
                    className={`native_receiver ${!isValid ? 'error-border' : ''}`}
                    style={{
                        border: '2px solid rgb(77, 144, 254)',
                        padding: '5px',
                        height: '250px',
                        width: '250px',
                    }}
                    contentEditable={true}
                    onInput={this.handleChange}
                    onBlur={this.handleChange}
                    suppressContentEditableWarning={true}
                >
                </div>
                <div style={{ textAlign: 'center', padding: '20px' }}>
                    <button style={{ padding: '10px' }} onClick={this.addNewLink}>
                        {"Add Link"}
                    </button>
                </div>
            </div>
        )

    }


}

function addClass(elem, className) {
    var classNames = elem.className.split(" ")
    if (classNames.indexOf(className) === -1) {
        classNames.push(className)
    }
    elem.className = classNames.join(" ")
}
function selectElem(selector) {
    return document.querySelector(selector)
}
function selectAllElems(selector) {
    return document.querySelectorAll(selector)
}
function removeElem(elem) {
    return elem ? elem.parentNode.removeChild(elem) : false
}


function nativeBindDraggable(elems = false) {
    elems = elems || selectAllElems('.native_drag');
    if (!elems) {
        // No element exists, abort
        return false;
    } else if (elems.outerHTML) {
        // if only a single element, put in array
        elems = [elems];
    }
    // else it is html-collection already (as good as array)

    for (let i = 0; i < elems.length; i++) {
        // For every elem in list, attach or re-attach event handling
        elems[i].dataset.transferreference = `transit_${new Date().getTime()}`;
        elems[i].ondragstart = function (e) {
            if (!e.target.id) {
                e.target.id = (new Date()).getTime();
            }

            window.inTransferMarkup = e.target.outerHTML.replace("dragged", "");
            window.transferreference = elems[i].dataset.transferreference;
            addClass(e.target, 'dragged');
        };
    };
}

function nativeBindWriteRegion(elems = false) {
    elems = elems || selectAllElems('.native_receiver');
    if (!elems) {
        // No element exists, abort
        return false;
    } else if (elems.outerHTML) {
        // if only a single element, put in array
        elems = [elems];
    }
    // else it is html-collection

    for (let i = 0; i < elems.length; i++) {
        elems[i].ondragover = function (e) {
            e.preventDefault();
            return false;
        };
        elems[i].ondrop = function (e) {
            receiveBlock(e);
        };
    }
}

function receiveBlock(e) {
    e.preventDefault();
    let content = window.inTransferMarkup;

    window.inTransferMarkup = "";

    let range = null;
    if (document.caretRangeFromPoint) { // Chrome
        range = document.caretRangeFromPoint(e.clientX, e.clientY);
    } else if (e.rangeParent) { // Firefox
        range = document.createRange();
        range.setStart(e.rangeParent, e.rangeOffset);
    }

    if (range == null)
        return false;

    if (range.endOffset === 0 && range.startOffset === 0) {
        range.endContainer.parentNode.classList.remove("dragged")
        return false
    }



    if (range.endContainer.parentNode.classList.contains("native_drag") === true) {
        range.endContainer.parentNode.classList.remove("dragged")
        return false
    }

    let sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
    e.target.focus();

    document.execCommand('insertHTML', false, content);
    sel.removeAllRanges();

    // reset draggable on all blocks, esp the recently created
    nativeBindDraggable(
        document.querySelector(
            `[data-transferreference='${window.transferreference}']`
        )
    );

    removeElem(selectElem('.dragged'));

    var native_receiverElement = document.getElementById("native_receiver");

    if (native_receiverElement.lastElementChild.tagName.toLowerCase() !== "br") {
        var lineBreakNode = document.createElement("br");
        native_receiverElement.appendChild(lineBreakNode);
    }

    return false;
}

function isValidMessageText(value) {
    const messageRegex = /[~^|;©\{\}\[\]\\]/gi
    return !messageRegex.test(value)
}